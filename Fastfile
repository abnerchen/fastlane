# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:ios)

platform :ios do

  before_all do |lane, options|
    if lane != :upload_bugly
      cocoapods(
      	repo_update: true,
      	use_bundle_exec: false
      )
    end
  end

  after_all do |lane, options|
    sh "echo 操作完成"
    # sh "git checkout ./"
  end

  lane :build do |options|

    # 以fastlane文件夹目录为根目录
    package_path = options[:package_path] ? options[:package_path] : "./package_page.json"
    package_page_file = File.read(package_path)
    package_page = JSON.parse(package_page_file)

    # 转为绝对路径，防止文件路径转换造成读取文件失败
    xcworkspace_path_relatively = package_page["xcworkspace_path"]
    xcworkspace_path_absolutely = File.expand_path(xcworkspace_path_relatively)

    scheme = package_page["scheme"]

    xcodebuild(
      workspace: xcworkspace_path_absolutely,
      scheme: scheme,
      configuration: "Debug",
      clean: true,
      build: true,
      simulator: false,
      build_settings: {
        "CODE_SIGNING_REQUIRED" => "NO",
        "CODE_SIGN_IDENTITY" => ""
      }
    )
  end

  # 更新app_macro
  def update_app_macro(macro, is_app_store)
    file_path = macro["file_path"];
    channnel_name = macro["kchannelId"];
    api_server = macro["kAPI_USER_SERVER"]
    
    is_formal = 0
    if macro.has_key?("IS_FORMAL")
      is_formal = macro["IS_FORMAL"]
    else  
      is_formal = is_app_store ? 1 : 0
    end

    sh "sed -i 's/#define kchannelId.*/#define kchannelId @\"#{channnel_name}\"/g'  #{file_path}"
    sh "sed -i 's/#define kAPI_USER_SERVER.*/#define kAPI_USER_SERVER @\"#{channnel_name}\"/g'  #{file_path}"

  end

  # 更新infoPlist
  def update_info_plist(infoPlist_path, app_name, build_number, bundle_identifier)
    # 更新infoPlist
    # set_info_plist_value(path: infoPlist, key: "CFBundleShortVersionString", value: version)
    if build_number
      set_info_plist_value(path: infoPlist_path, key: "CFBundleVersion", value: build_number)
    end

    # 设置CFBundleIdentifier 和 xcodeproj关联
    if bundle_identifier
      set_info_plist_value(path: infoPlist_path, key: "CFBundleIdentifier", value: bundle_identifier)
    else
      set_info_plist_value(path: infoPlist_path, key: "CFBundleIdentifier", value: "$(PRODUCT_BUNDLE_IDENTIFIER)")
    end

    # 设置显示的app名字
    if app_name
      set_info_plist_value(path: infoPlist_path, key: "CFBundleDisplayName", value: app_name)
    end
  end

  # 记录上传dSYM文件的命令, 到打包目录
  def save_ipa_info_for_bugly(product_version, bundle_id)

  end

  lane :package do | options |
    fastlane_require 'json'
    fastlane_require 'uri'
    fastlane_require 'net/https'

    build_number = options[:build_number]
    method = options[:method]
    # 以fastlane文件夹目录为根目录
    package_path = options[:package_path] ? options[:package_path] : "./package_page.json"
    package_page_file = File.read(package_path)
    package_page = JSON.parse(package_page_file)

    # 转为绝对路径，防止文件路径转换造成读取文件失败
    xcworkspace_path_relatively = package_page["xcworkspace_path"]
    xcworkspace_path_absolutely = File.expand_path(xcworkspace_path_relatively)
    xcodeproj_path_relatively = package_page["xcodeproj_path"]
    xcodeproj_path_absolutely = File.expand_path(xcodeproj_path_relatively)

    scheme = package_page["scheme"]
    package = package_page["package"]
    page = package[method]
    
    
      app_display_name = page["app_display_name"]

      if page.has_key?("not_package_me") && (page["not_package_me"] == 1)
        puts "not_package_me: 跳过#{app_display_name}打包"
        next
      end
      if page.has_key?("package_me") && (page["package_me"] == false)
        puts "package_me: 跳过#{app_display_name}打包"
        next
      end


      export_method = page["export_method"]
      update_target_signs = page["update_target_signs"]
      team_id = page["team_id"]

      # 更新macro
      change_app_macro = page["change_app_macro"]
      update_app_macro(change_app_macro, (export_method == "app-store"))

      # 更新sign
      first_bundle_identifier = nil
      update_target_signs.each do | sign_page |
        each_bundle_identifier = sign_page["bundle_identifier"]

        if first_bundle_identifier.nil? 
          first_bundle_identifier = each_bundle_identifier
        end

        # 读取info_plist
        info_plist_path_relatively = sign_page["info_plist_path"]
        info_plist_path_absolutely = File.expand_path(info_plist_path_relatively)

        update_target_sign(
          xcodeproj_path: xcodeproj_path_absolutely,
          target_name: sign_page["target_name"],
          team_id: team_id,
          bundle_identifier: each_bundle_identifier,
          provisioning_profile_specifier: sign_page["provisioning_profile_specifier"]
        )

        # 更新info.Plist
        update_info_plist(info_plist_path_absolutely, app_display_name, build_number, each_bundle_identifier)

        # 将kchannelId 打入infoPlist
        kchannelId = change_app_macro["kchannelId"];
        set_info_plist_value(path: info_plist_path_absolutely, key: "CUSTOM_kchannelId", value: kchannelId)
      end
      
      # 编译打包
      output_name = "#{first_bundle_identifier}.ipa"
      if page.has_key?("package_name")
        output_name = page["package_name"]
        puts "使用#{output_name}包名"
      end

      expand_path = File.expand_path(xcworkspace_path_absolutely)
      output_directory = File.expand_path("../outpath/#{method}")

      build_ios_app(
        scheme: scheme,
        workspace: expand_path,
        configuration: "Release",
        export_method: export_method, # Method used to export the archive. Valid values are: app-store, ad-hoc, package, enterprise, development, developer-id
        clean: true,
        output_directory: output_directory, # Destination directory. Defaults to current directory.
        output_name: output_name,       # specify the name of the .ipa file to generate (including file extension)
        include_bitcode: false,
        export_options: {
          method: export_method,
          simulator: false 
        }
      )
    
  end	

  lane :upload_bugly do | options |

    app_key = options[:app_key]
    app_id = options[:app_id]
    ipa_path = options[:ipa_path] # 绝对路径

    ipa_dir = File::dirname(ipa_path)
    Dir.chdir(ipa_dir) do
      puts "跳转到ipa目录：#{ipa_dir}"
    
      kchannelId = get_ipa_info_plist_value(ipa: ipa_path, key: "CUSTOM_kchannelId")
      puts kchannelId
      product_version = get_ipa_info_plist_value(ipa: ipa_path, key: "CFBundleShortVersionString")
      puts product_version
      bundle_identifier = get_ipa_info_plist_value(ipa: ipa_path, key: "CFBundleIdentifier")
      puts bundle_identifier

      require 'net/https'
      require 'uri'
      params = {} 
      params["api_version"] = '1'
      params["app_id"] = "#{app_id}"
      params["app_key"] = "#{app_key}"
      params["symbolType"] = '2'
      params["bundleId"] = "#{bundle_identifier}"
      params["productVersion"] = "#{product_version}"
      params["channel"] = "#{kchannelId}"
      params["fileName"] = "#{bundle_identifier}.app.dSYM.zip"
      params["file"] = "@#{bundle_identifier}.app.dSYM.zip"
      puts params

      uri = URI.parse("https://api.bugly.qq.com/openapi/file/upload/symbol?app_key=#{app_key}&app_id=#{app_id}")
      res = Net::HTTP.post_form(uri, params)  

      if res.code.to_i != 200
        #返回的html body 
        puts res.code
        puts res.body
        raise "上传出错" 
      end

    end
  end
end
