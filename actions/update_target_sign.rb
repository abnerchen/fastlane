require 'xcodeproj'

module Fastlane
  module Actions
    module SharedValues
      UPDATE_TARGET_SIGN_CUSTOM_VALUE = :UPDATE_TARGET_SIGN_CUSTOM_VALUE
    end

    class UpdateTargetSignAction < Action
      def self.run(params)
          
        xcodeproj_path = params[:xcodeproj_path]
        target_name = params[:target_name]
        team_id = params[:team_id]
        bundle_identifier = params[:bundle_identifier]
        provisioning_profile_specifier = params[:provisioning_profile_specifier]

        has_changed = false
        project = Xcodeproj::Project.open(xcodeproj_path)
        project.targets.each do |target|
            if target.name == target_name
                target.build_configurations.each do |config|

                  config.build_settings["PRODUCT_BUNDLE_IDENTIFIER"] = bundle_identifier
                  config.build_settings["DEVELOPMENT_TEAM"] = team_id
                  
                    if config.name == 'Release'
                        config.build_settings["PROVISIONING_PROFILE_SPECIFIER"] = provisioning_profile_specifier
                        config.build_settings["PROVISIONING_PROFILE"] = ""
                        config.build_settings["CODE_SIGN_IDENTITY"] = ""
                        config.build_settings["CODE_SIGN_IDENTITY[sdk=iphoneos*]"] = "iPhone Distribution";
                        
                        has_changed = true
                    end
                end
            end
        end
        project.save
        if has_changed
          UI.message "success update 🎉"
        else
          UI.message "fail update ❌"
        end
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "manual update someone target sign for build and package"
      end

      def self.details
        "手动替换某个target的sign参数，以达到动态替换，解决多渠道多target打包问题, 如果不是指定的绝对路径，则相对路径为fastlane文件夹所在目录"
      end

      def self.available_options
        # Define all options your action supports. 
        [
          FastlaneCore::ConfigItem.new(key: :xcodeproj_path,
                                       env_name: "xcodeproj_path",
                                       description: "xcodeproj path",
                                       is_string: true,
                                       default_value: false,
                                       verify_block: proc do |value|
                                        sh("pwd")
                                        UI.user_error!("Couldn't find file at path '#{value}'") unless File.exist?(value)
                                       end),
          FastlaneCore::ConfigItem.new(key: :target_name,
                                       description: "someone targe name",
                                       is_string: true,
                                       default_value: false),
          FastlaneCore::ConfigItem.new(key: :team_id,
                                       description: "need changed team id",
                                       is_string: true,
                                       default_value: false),
          FastlaneCore::ConfigItem.new(key: :bundle_identifier,
                                       description: "need changed bundle identifier",
                                       is_string: true,
                                       default_value: false),
          FastlaneCore::ConfigItem.new(key: :provisioning_profile_specifier,
                                       description: "need changed provisioning profile name for this target",
                                       is_string: true,
                                       default_value: false),
        ]
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        # ["Your GitHub/Twitter Name"]
        ["zhihui.li@yingzt.com"]
      end

      def self.is_supported?(platform)
        true
      end
    end
  end
end
